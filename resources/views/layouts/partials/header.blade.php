<div class="sidebar-toggle-box">
    <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
</div>
<!--logo start-->
<a href="index.html" class="logo"><b>DASH<span>IO</span></b></a>
<!--logo end-->
<div class="nav notify-row" id="top_menu">
    <!--  notification start -->
  
    <!--  notification end -->
</div>
<div class="top-menu">
    <ul class="nav pull-right top-menu">
        <li>
            <a class="logout" href="login.html" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
    </ul>
</div>