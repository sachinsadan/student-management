<ul class="sidebar-menu" id="nav-accordion">
    {{-- <p class="centered">
        <a href="profile.html"><img src="img/ui-sam.jpg" class="img-circle" width="80"></a>
    </p> --}}
     @if(\Auth::user())
        <h5 class="centered">{{ \Auth::user()->name }}</h5>
    @endif
    <li class="mt">
        <a class="sub-menu" href="{{ route('teachers.list') }}">
            <i class="fa fa-dashboard"></i>
            <span>Teachers</span>
        </a>
    </li>
    <li>
        <a class="sub-menu" href="{{ route('students.list') }}">
            <i class="fa fa-user"></i>
            <span>Students</span>
        </a>
    </li>
    <li>
        <a class="sub-menu" href="{{ route('terms.list') }}">
            <i class="fa fa-desktop"></i>
            <span>Terms</span>
        </a>
    </li>
    <li>
        <a class="sub-menu" href="{{ route('marks.list') }}">
            <i class="fa fa-book"></i>
            <span>Student Marks</span>
        </a>
    </li>
    
</ul>
