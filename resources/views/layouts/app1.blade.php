<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <title>Student Management</title>

    <!-- Favicons -->
    <link href="{{asset('img/favicon.png')}}" rel="icon">
    <link href="{{asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

    <!-- Bootstrap core CSS -->
    <link href="{{asset('admin_theme/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('admin_theme/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{asset('admin_theme/css/zabuto_calendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_theme/lib/gritter/css/jquery.gritter.css')}}" />
    <!-- Custom styles for this template -->
    <link href="{{asset('admin_theme/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('admin_theme/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin_theme/css/style-responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('admin_theme/lib/jquery-ui/jquery-ui.min.css')}}" />
    <script src="{{asset('admin_theme/lib/chart-master/Chart.js')}}"></script>
    <link rel="stylesheet" href="{{asset('admin_theme/lib/dropzone.css')}}" />
    <script src="{{asset('admin_theme/lib/dropzone.js')}}"></script>


    <!-- Bootstrap core CSS -->
    <link href="{{asset('admin_theme/lib/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('admin_theme/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{asset('admin_theme/lib/advanced-datatable/css/demo_page.css')}}" rel="stylesheet" />
    <link href="{{asset('admin_theme/lib/advanced-datatable/css/demo_table.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('admin_theme/lib/advanced-datatable/css/DT_bootstrap.css')}}" />
    <!-- Custom styles for this template -->
    <link href="{{asset('admin_theme/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('admin_theme/css/style-responsive.css')}}" rel="stylesheet">   
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    @stack('head')
    
</head>

<body>
    <section id="container">
        <!--header start-->
        <header class="header black-bg">
            @include('layouts.partials.header')
        </header>
        <!--header end-->
       
        <!--sidebar start-->
        <aside>
            <div id="sidebar" class="nav-collapse ">
                <!-- sidebar menu start-->
                @include('layouts.partials.side_menu')
                <!-- sidebar menu end-->
            </div>
        </aside>
        <!--sidebar end-->
        
        <!--main content start-->
        <section id="main-content">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-12 main-chart">
                        <!--CUSTOM CHART START -->
                        <div class="border-head">
                            {{-- <h3>Admin Dashboard</h3> --}}
                        </div>
                        <div class="content">
                            <!-- flash message -->
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                            @endif
  
                            @if ($message = Session::get('error'))
                                <div class="alert alert-danger alert-block">
                                    <button type="button" class="close" data-dismiss="alert">×</button>    
                                    <strong>{{ $message }}</strong>
                                </div>
                                @endif

                            <!-- flash message ends-->
   
                            @yield('content')
                        </div>
                       
                    </div>
                    <!-- /col-lg-9 END SECTION MIDDLE -->
                    <!-- /col-lg-3 -->
                </div>
                <!-- /row -->
            </section>
        </section>
        <!--main content end-->

    </section>
    <script src="{{asset('admin_theme/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('admin_theme/lib/jquery-ui/jquery-ui.min.js')}}"></script>
    <script src="{{asset('admin_theme/lib/bootstrap/js/bootstrap.min.js')}}"></script>
    <script class="include" type="text/javascript" src="{{asset('admin_theme/lib/jquery.dcjqaccordion.2.7.js')}}"></script>
    <script src="{{asset('admin_theme/lib/jquery.scrollTo.min.js')}}"></script>
    <script src="{{asset('admin_theme/lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin_theme/lib/jquery.sparkline.js')}}"></script>
    <!--common script for all pages-->
    <script src="{{asset('admin_theme/lib/common-scripts.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_theme/lib/gritter/js/jquery.gritter.js')}}"></script>
    <script type="text/javascript" src="{{asset('admin_theme/lib/gritter-conf.js')}}"></script>
    <!--script for this page-->
    <script src="{{asset('admin_theme/lib/sparkline-chart.js')}}"></script>
    <script src="{{asset('admin_theme/lib/zabuto_calendar.js')}}"></script>
    <script type="text/javascript">
         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('[name="_token"]').attr('content')
            }
        });
        $( document ).ajaxStart(function() {
                $( "#loading" ).show();
        });

        $( document ).ajaxComplete(function() {
            $( "#loading" ).hide();
        });
    </script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js"></script>
  <script src="{{asset('admin_theme/lib/jquery/jquery.min.js')}}"></script>
  <script type="text/javascript" language="javascript" src="{{asset('admin_theme/lib/advanced-datatable/js/jquery.js')}}"></script>
  <script src="{{asset('admin_theme/lib/bootstrap/js/bootstrap.min.js')}}"></script>
  <script class="include" type="text/javascript" src="{{asset('admin_theme/lib/jquery.dcjqaccordion.2.7.js')}}"></script>
  <script src="{{asset('admin_theme/lib/jquery.scrollTo.min.js')}}"></script>
  <script src="{{asset('admin_theme/lib/jquery.nicescroll.js')}}" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="{{asset('admin_theme/lib/advanced-datatable/js/jquery.dataTables.js')}}"></script>
  <script type="text/javascript" src="{{asset('admin_theme/lib/advanced-datatable/js/DT_bootstrap.js')}}"></script>
  <!--common script for all pages-->
  {{-- <script src="{{asset('lib/common-scripts.js')}}"></script> --}}

   {{-- bootstrap toggle --}}
   <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">

// URL to simulate success
const success = 'https://codepen.io/uxjulia/pen/a35d3ea1688ec7d933f257f9ffa67116.html'
// URL to simulate Error
const fail = 'https://codepen.io/uxjulia/pen/d88ca4d7142a75937092ed02e8ddbcb1.html'
  $(document).ready(function() {
    /*
     * Insert a 'details' column to the table
     */
    var nCloneTh = document.createElement('th');
    var nCloneTd = document.createElement('td');
    nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
    nCloneTd.className = "center";

    $('#hidden-table-info thead tr').each(function() {
      this.insertBefore(nCloneTh, this.childNodes[0]);
    });

    $('#hidden-table-info tbody tr').each(function() {
      this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
    });

    /*
     * Initialse DataTables, with no sorting on the 'details' column
     */
    var oTable = $('#hidden-table-info').dataTable({
      "aoColumnDefs": [{
        "bVisible": false,
        "aTargets": [0]
      }],
      "aaSorting": [
        [1, 'asc']
      ]
    });

    /* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */
    $('#hidden-table-info tbody td img').remove();


  });
</script>

    @stack('scripts')

</body>

</html