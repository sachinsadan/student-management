@extends('layouts.app1')

@section('content')
  <h3><i class="fa fa-angle-right"></i> Student Marks</h3> <span><a href="{{ route('marks.create') }}" class="btn btn-success"  style="float: right; margin-top: -38px; margin-right: 15px;" >Create New</a></span>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th> ID</th>
                    <th> Name</th>
                    <th> Maths</th>
                    <th> Science</th>
                    <th> History</th>
                    <th> Term</th>
                    <th> Total Marks</th>
                    <th> Created On</th>
                    <th> Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($marks as $mark)
                  <tr>
                      <td>{{$mark->id}}</td>
                      <td>{{$mark->getStudent->name}}</td>
                      <td>{{$mark->maths}}</td>
                      <td>{{$mark->science}}</td>
                      <td>{{$mark->history}}</td>
                      <td>{{$mark->getTerm->term}}</td>
                        @php
                        $total = 0;
                        $total = (int)$mark->maths + (int)$mark->science + (int)$mark->history;
                        @endphp
                      <td>{{$total}}</td>
                      <td>{{ date("F j, Y, g:i a", strtotime($mark->created_at))  }}</td>
                      <td>
                        <a href="{{ route('marks.edit',$mark->id) }}" class="btn btn-primary btn-xs" title="edit user"><i class="fa fa-edit"></i></a>
                        <a href="{{ route('marks.delete',$mark->id ) }}" class="btn btn-danger btn-xs" title="delete user" style="margin-left: 10px;" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i></a>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
@endsection
