@extends('layouts.app1')

@section('content')
<div class="row mt">
    <div class="col-lg-12">
        <h4><i class="fa fa-angle-right"></i> Update Marks</h4>
        <div class="form-panel"> 
            <div class=" form">
                <form class="cmxform form-horizontal style-form" method="POST" action="{{ route('marks.update', $marks->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group ">
                        <label for="student_id" class="control-label col-lg-2">Student ID<span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <select class=" form-control" id="student_id" name="student_id" required >
                                <option value="">Select Student</option>
                                @foreach($students as $student)
                                    <option value="{{ $student->id }}" {{ $marks->student_id == $student->id ? 'selected' : '' }}>{{ $student->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="maths" class="control-label col-lg-2">Maths <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input value="{{ $marks->maths }}" class=" form-control" id="maths" name="maths" minlength="2" type="number" required />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="science" class="control-label col-lg-2">Science <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input value="{{ $marks->science }}" class=" form-control" id="science" name="science" minlength="2" type="number" required />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="history" class="control-label col-lg-2">History <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input  value="{{ $marks->history }}" class=" form-control" id="history" name="history" minlength="2" type="number" required />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="term_id" class="control-label col-lg-2">Term <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <select class=" form-control" id="term_id" name="term_id" required >
                                <option value="">Select Term</option>
                                @foreach($terms as $term)
                                    <option value="{{ $term->id }}" {{ $marks->term_id == $term->id ? 'selected' : '' }}>{{ $term->term }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-theme" type="submit">Submit</button>
                            <a href="{{ route('marks.list') }}" class="btn btn-theme04" type="button">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /form-panel -->
    </div>
    <!-- /col-lg-12 -->
</div>
@endsection
