@extends('layouts.app1')

@section('content')
<div class="row mt">
    <div class="col-lg-12">
        <h4><i class="fa fa-angle-right"></i> Update Student</h4>
        <div class="form-panel"> 
            <div class=" form">
                <form class="cmxform form-horizontal style-form" method="POST" action="{{ route('students.update', $student->id) }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group ">
                        <label for="title" class="control-label col-lg-2">Name <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input value="{{ $student->name }}"  class=" form-control" id="title" name="name" minlength="2" type="text" required />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="age" class="control-label col-lg-2">Age <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input value="{{ $student->age }}" class=" form-control" id="age" name="age" minlength="2" type="number" required />
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="gender" class="control-label col-lg-2">Gender <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <select class=" form-control" id="gender" name="gender" required >
                                <option value="">Select Gender</option>
                                <option value="Male" {{ $student->gender == 'Male' ? 'selected' : '' }}>Male</option>
                                <option value="Female" {{ $student->gender == 'Female' ? 'selected' : '' }}>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="teacher_id" class="control-label col-lg-2">Reporting Teacher <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <select class=" form-control" id="teacher_id" name="teacher_id" required >
                                <option value="">Select Reporting Teacher</option>
                                @foreach($teachers as $teacher)
                                    <option value="{{ $teacher->id }}" {{ $student->teacher_id == $teacher->id ? 'selected' : '' }}>{{ $teacher->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-theme" type="submit">Submit</button>
                            <a href="{{ route('students.list') }}" class="btn btn-theme04" type="button">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /form-panel -->
    </div>
    <!-- /col-lg-12 -->
</div>
@endsection
