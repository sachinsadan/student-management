@extends('layouts.app1')

@section('content')
  <h3><i class="fa fa-angle-right"></i> Teachers</h3> <span><a href="{{ route('teachers.create') }}" class="btn btn-success"  style="float: right; margin-top: -38px; margin-right: 15px;" >Create New</a></span>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th><i class="fa fa-bullhorn"></i> Name</th>
                    <th><i class=" fa fa-universal-access"></i> Email</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($teachers as $teacher)
                  <tr>
                      <td>{{$teacher->name}}</td>
                      <td>{{$teacher->email}}</td>
                      <td>
                      <a href="{{ route('teachers.edit',$teacher->id) }}" class="btn btn-primary btn-xs" title="edit user"><i class="fa fa-edit"></i></a>
                      <a href="{{ route('teachers.delete',$teacher->id ) }}" class="btn btn-danger btn-xs" title="delete user" style="margin-left: 10px;" onclick="return confirm('Are you sure you want to delete this user?');"><i class="fa fa-trash"></i></a>
                      </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
@endsection

