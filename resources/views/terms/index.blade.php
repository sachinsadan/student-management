@extends('layouts.app1')

@section('content')
  <h3><i class="fa fa-angle-right"></i> Terms</h3> <span><a href="{{ route('terms.create') }}" class="btn btn-success"  style="float: right; margin-top: -38px; margin-right: 15px;" >Create New</a></span>
        <div class="row mb">
          <!-- page start-->
          <div class="content-panel">
            <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr>
                    <th> ID</th>
                    <th> Name</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($terms as $term)
                  <tr>
                      <td>{{$term->id}}</td>
                      <td>{{$term->term}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
@endsection

