@extends('layouts.app1')

@section('content')
<div class="row mt">
    <div class="col-lg-12">
        <h4><i class="fa fa-angle-right"></i> Create New Term</h4>
        <div class="form-panel"> 
            <div class=" form">
                <form class="cmxform form-horizontal style-form" method="POST" action="{{ route('terms.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group ">
                        <label for="term" class="control-label col-lg-2">Term <span style="color: red;">*</span></label>
                        <div class="col-lg-10">
                            <input class=" form-control" id="term" name="term" minlength="2" type="text" required />
                        </div>
                    </div>          
                    
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button class="btn btn-theme" type="submit">Submit</button>
                            <a href="{{ route('terms.list') }}" class="btn btn-theme04" type="button">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /form-panel -->
    </div>
    <!-- /col-lg-12 -->
</div>
@endsection
