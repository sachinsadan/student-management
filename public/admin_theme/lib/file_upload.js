$(document).ready(function() {
    $(document).on('change', '#file-input', function() {
        var $preview = $('#preview').empty();
        if (this.files) $.each(this.files, readAndPreview);
        var elem = [];
        var images = [];

        function readAndPreview(i, file) {
            if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                $('#loading_div').hide();
                return alert(file.name + " is not an image");
            } else {
                var reader = new FileReader();
                var formDataImage = new FormData();
                var test = $("#file-input")[0].files[0];
                formDataImage.append('attachment', test);
                $(reader).on("load", function() {
                    $('.upload_status').text('Uploading..')
                    $('.modal_upload').addClass('hidden');
                    $('.modal_preview').removeClass('hidden');
                    // console.log(this.result)
                    elem.push('<div class="preview_img_container" style="postion: relative; padding-top: 25px;"><div class="hover_close_parent"><img src="' + this.result + '" class="modal_preview_img"><a class="remImage" href="#" id="delete_image"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div><div class="modal_preview_filename text_wrap" title="' + file.name + '" style="position: relative; padding-top: 43px;">' + file.name + '</div></div>')
                    $('.modal_preview').html(elem)

                });
                reader.readAsDataURL(file);
            }
        }
    });

    $(document).on('click', '#delete_image', function() {
        $('.modal_preview').empty();
        $('.modal_preview').addClass('hidden');
        $('.modal_upload').removeClass('hidden');
    });
});