After cloning this codes succesfully, Please follow the following instructions.

1. Create a database named "student_management_system" (if database with another name is connected run the comman "php artisan config:cache" and "php artisan cache:clear")
2. Run the command "php artisan migrate"
3. Run the command "php artisan db:seed"
    Then you can login as a user "admin@mailinator.com" with password "123456" or you can register as a new user
    (password should be more than 8 characters)

4. Create teachers
5. Then create students, now you can select a reporting teacher
6. create terms
7. Now you will be able to add students marks with student and term selectable and see the list of students with their marks
