<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['name', 'age', 'gender', 'teacher_id'];

    public function getTeacher()
    {
        return $this->hasOne(\App\Models\Teacher::Class, 'id', 'teacher_id');
    }
}
