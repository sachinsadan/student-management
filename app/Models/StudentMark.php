<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentMark extends Model
{
    protected $fillable = ['maths', 'science', 'history', 'term_id', 'student_id'];

    public function getStudent()
    {
        return $this->hasOne(\App\Models\Student::Class, 'id', 'student_id');
    }

    public function getTerm()
    {
        return $this->hasOne(\App\Models\Term::Class, 'id', 'term_id');
    }
}
