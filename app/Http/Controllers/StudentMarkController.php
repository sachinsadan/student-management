<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentMark;
use App\Models\Student;
use App\Models\Term;

class StudentMarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marks = StudentMark::all();
        return view('marks.index', compact('marks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $students = Student::all();
        $terms = Term::all();
        return view('marks.create', compact('students', 'terms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'student_id' => 'required',
            'maths' => 'required|integer',
            'science' => 'required|integer',
            'history' => 'required|integer',
            'term_id' => 'required',
        ]);
        StudentMark::create($validated);
        return redirect()->route('marks.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $students = Student::all();
        $terms = Term::all();
        $marks = StudentMark::find($id);
        return view('marks.edit', compact('students', 'terms', 'marks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'student_id' => 'required',
            'maths' => 'required|integer',
            'science' => 'required|integer',
            'history' => 'required|integer',
            'term_id' => 'required',
        ]);
        $marks = StudentMark::find($id);
        $marks->update($validated);
        return redirect()->route('marks.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        StudentMark::find($id)->delete();
        return redirect()->route('marks.list');
    }
}
