<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


// Teachers
Route::get('teachers/list', 'TeacherController@index')->name('teachers.list');
Route::get('teachers/create', 'TeacherController@create')->name('teachers.create');
Route::get('teachers/edit/{id}', 'TeacherController@edit')->name('teachers.edit');
Route::get('teachers/delete/{id}', 'TeacherController@destroy')->name('teachers.delete');
Route::post('teachers/store', 'TeacherController@store')->name('teachers.store');
Route::post('teachers/update/{id}', 'TeacherController@update')->name('teachers.update');


// Students
Route::get('students/list', 'StudentController@index')->name('students.list');
Route::get('students/create', 'StudentController@create')->name('students.create');
Route::post('students/store', 'StudentController@store')->name('students.store');
Route::get('students/edit/{id}', 'StudentController@edit')->name('students.edit');
Route::get('students/delete/{id}', 'StudentController@destroy')->name('students.delete');
Route::post('students/update/{id}', 'StudentController@update')->name('students.update');


// Terms
Route::get('terms/list', 'TermController@index')->name('terms.list');
Route::get('terms/create', 'TermController@create')->name('terms.create');
Route::post('terms/store', 'TermController@store')->name('terms.store');


// Student Marks
Route::get('home', 'StudentMarkController@index')->name('marks.list');
Route::get('marks/create', 'StudentMarkController@create')->name('marks.create');
Route::post('marks/store', 'StudentMarkController@store')->name('marks.store');
Route::get('marks/edit/{id}', 'StudentMarkController@edit')->name('marks.edit');
Route::get('marks/delete/{id}', 'StudentMarkController@destroy')->name('marks.delete');
Route::post('marks/update/{id}', 'StudentMarkController@update')->name('marks.update');



