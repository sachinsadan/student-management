<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = ['name' => 'Admin', 'email' => 'admin@mailinator.com', 'password' => bcrypt('123456')];

        User::create($admin);
    }
}
