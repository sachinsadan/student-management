<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_marks', function (Blueprint $table) {
            $table->id();
            $table->string('maths');
            $table->string('science');
            $table->string('history');
            $table->unsignedBigInteger('term_id')->nullable()->comment('Ref ID to terms table');
            $table->foreign('term_id')->nullable()->references('id')->on('terms');
            $table->unsignedBigInteger('student_id')->nullable()->comment('Ref ID to students table');
            $table->foreign('student_id')->nullable()->references('id')->on('students');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_marks');
    }
}
